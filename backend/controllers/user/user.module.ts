import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { AuthController } from '../auth/auth.controller';
import { AuthService } from '../auth/auth.service';

@Module({
  controllers: [UserController, AuthController],
  providers: [UserService, AuthService],
})
export class UserModule {}
