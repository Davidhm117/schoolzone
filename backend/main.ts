import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { config } from './config';
import { AppModule } from './app.module';
import { GlobalInterceptor } from './handlers/interceptors/global.interceptor';

async function bootstrap() {
  const { appName, port, version } = config;
  const app = await NestFactory.create(AppModule);
  const url = `/${version}`;

  const options = new DocumentBuilder()
    .addServer(`${url}`)
    .setTitle(appName)
    .setDescription('The Api documentation')
    .setVersion(version)
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('swagger', app, document);

  app.setGlobalPrefix(version);
  app.useGlobalInterceptors(new GlobalInterceptor());

  await app.listen(port, () => {
    console.info(`${appName} is running in http://localhost:${port}${url}`);
  });
}
bootstrap();
