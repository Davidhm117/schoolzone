# Schoolzone

#TODO list

- Initialize Angular frontend
  - Actual school App
  - School Admin App
  - Product Admin App
  - Sandbox App

# Terminal Commands Used

- ng new school --skipInstall=true --minimal=true --createApplication=false --skipGit=true --style=scss --skipTests=true --prefix=sz --packageManager=yarn --newProjectRoot=frontend --directory=./

- ng g application school --minimal=true --routing --style=scss --prefix=sz
  --skipTests=true
- ng g application admin --minimal=true --routing --style=scss --prefix=sz
  --skipTests=true
- ng g application super --minimal=true --routing --style=scss --prefix=sz
  --skipTests=true
- ng g application sandbox --minimal=true --routing --style=scss --prefix=sz
  --skipTests=true

# Lecture 22

- Initialize Angular frontend
